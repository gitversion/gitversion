package org.gitversion.util

/**
 * Not a unit test (when I have better access to a linux env I'll think about
 * a way to make this work x-platform.)
 */
class GitExecutoryDevHarness{

  // current windows machine has no git on OS path and is
  // install at C:\Program Files\Git\bin\git.exe
  static void main(String[] args){
    println "here"

    GitExecutor exec = new GitExecutor(
      log: new GvLogger(debug: true),
    )
    assert exec.findGitBinary() == "C:\\PROGRA~1\\Git\\bin\\git"
    println "end test\n"


    exec = new GitExecutor(
      log: new GvLogger(debug: true),
      providedGitBinary: "git",
      forceProvidedGitBinary: true
    )
    assert exec.findGitBinary() == "git"
    println "end test\n"


    exec = new GitExecutor(
      log: new GvLogger(debug: true),
      providedGitBinary: "git",
    )
    assert exec.findGitBinary() == "C:\\PROGRA~1\\Git\\bin\\git"
    println "end test\n"


    exec = new GitExecutor(
      log: new GvLogger(debug: true),
      versionPrefix: 'version.'
    )
    println "describe: " + exec.describe
    println "end test\n"
  }


}
