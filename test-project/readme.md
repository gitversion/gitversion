This folder contains a test project to test out actual usage of the plugin.

It's a completely separate gradle project (not a sub-project).

It's not part of the plugin's build, but may depend on you having published
a build to the snapshot repository (install task)s o that it can use
it for testing.