[TOC]

# Introduction

Gitversion is a Gradle plugin that assists with doing version management via
the [git describe](https://git-scm.com/docs/git-describe) command.

# Pre-requisites

The plugin needs to be able to run the git binary.  This can present a problem
in two ways:

* if you don't have git on your path (see troubleshooting section)
* if you try to build in an environment without git 
    * this can happen in "continuous build" environments such as AWS
      Codebuild, Bitbucket pipelines etc.
    * the obvious workaround is to install git as part of your environment
      setup in your CD tool.
    * the plugin tries to use the git binary very early in the build 
      lifecycle (so the version can be used by other parts of the build) - 
      it needs to have some way of working without git being present
        

# Basic Usage

```
buildscript {
  repositories {
    jcenter()
  }
  dependencies {
    classpath 'org.gitversion:gitversion-plugin:x.y.z'
  }
}

apply plugin: 'org.gitversion.gitversion-plugin'

project.version = gitversion.version
```

Tag your project with a tag that looks like "version.0.0.1" and from then on
your project version while reflect its relative distance from the most recent
tag that starts with "version."

# Using the version number from code

Add the following to your Gradle project (below where you set project.version):
```
jar {
  manifest {
    attributes 'Implementation-Version': version
  }
}
```

Add a method like the following to any class that is packaged in the jar
built by Gradle:
```
public String getVersion(){
  return getClass().getPackage().getImplementationVersion();
}
```

# Customising the tag
Add the following to your build fuile (above where you use gitversion):
```
gitversion {
  versionPrefix = "my-project-"
}
```


# Specifying the git binary location
By default, the plugin just tries to execute "git".
If your git binary is on the path, then it should just be found (should
work on any OS).
If you don't like stuff on your global path (I'm fussy like that), you can set
a system property "gitversion.gitBinary".

I set the property for all gitversion projects by adding the following to my
~/.gradle/gradle.properties:
`systemProp.gitversion.gitBinary=C:/Progra~1/Git/bin/git`

You can do your own thing by adding a gitversion block to your project
(make sure you place this block in your build file above where you try to use
gitversion).

```
gitversion {
  gitBinary = "bizarre hardcoded path weirdness (whyyyy?)"
}
```


# Troubleshooting

## Debugging
If something goes wrong, try adding the following to your build file to see
what's going on (or run gradle with the "-d" option).
Useful for seeing the exact git command being issued.
```
gitversion {
  debug = true
}
```

## Shallow clones
gitversion doesn't work very well with shallow clones, see this stack
overflow question:
http://stackoverflow.com/questions/35691922/how-can-i-use-git-describe-match-sensibly-in-combination-with-a-shallow-clon


# More documentation / functionality

Start with the javadocs for the
[GitversionPluginExtension](./gitversion-plugin/src/main/groovy/org/gitversion/plugin/GitversionPluginExtension.groovy)
class

# Working on the gitversion project
[building gitversion](./doc/building.md)





