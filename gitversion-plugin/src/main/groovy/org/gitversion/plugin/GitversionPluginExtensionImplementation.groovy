package org.gitversion.plugin

import org.gitversion.util.GitExecutor
import org.gitversion.util.GvLogger
import org.gradle.api.Project

class GitversionPluginExtensionImplementation extends GitversionPluginExtension{

  // configured in init()
  GvLogger log
  Project project
  GitversionPlugin plugin
  GitExecutor executor


  private String describeCache
  private String commitIdCache


  void init(GitversionPlugin plugin, Project project){
    this.plugin = plugin
    this.project = project
    log = new GvLogger(debug: debug)
    executor = new GitExecutor(
      log: log,
      providedGitBinary: gitBinary,
      versionPrefix: versionPrefix,
    )
  }

  /**
   * Only uses annotated tags (created with "git tag -a") so that we can see
   * the metadata of tag creation (who, when, etc.)
   */
  String getVersion(){
    assert versionPrefix
    if( cacheGitCommitCommands && describeCache ){
      return describeCache
    }

    describeCache = executor.describe
    return describeCache
  }

  String getCommitId(){
    if( cacheGitCommitCommands && commitIdCache ){
      return commitIdCache
    }

    commitIdCache = executor.getCommitId(shortCommitId)
    if( indicateDirtyCommitId && dirty ){
      commitIdCache += dirtyCommitIdIndicator
    }
    return commitIdCache
  }

  /**
   * Returns true if version ends with "-dirty", meaning there are uncommitted
   * changes in the working directory.
   */
  boolean isDirty(){
    return version.endsWith("-dirty")
  }

  /**
   * !isDirty
   * @see #isDirty()
   */
  boolean isClean(){
    return !isDirty()
  }


}
